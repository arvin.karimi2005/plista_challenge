<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Product;
use App\Models\Category;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $categories = Category::factory()->count(10)->create();

      foreach ($categories as $category) {
        Product::factory()->count(20)->for($category)->create();
      }

        // $this->call('UsersTableSeeder');
    }
}
