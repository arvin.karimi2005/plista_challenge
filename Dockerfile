from php:8.0.2-alpine3.12
workdir /app

RUN docker-php-ext-install mysqli && docker-php-ext-install pdo_mysql

copy . .

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer && composer install --no-progress && composer du

RUN php artisan swagger-lume:generate

CMD php artisan migrate:fresh --seed && php -S 0.0.0.0:8000 -t public
