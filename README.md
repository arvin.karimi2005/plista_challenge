# Plista Challenge
This project is developed during the Plista hire Process, It's Simple Projects based on lumen:8.
There is a model named Product which has one to many relation with category model, and bunch of CRUD APIs.  

## Build
This project can be deployed with Docker and Docker-Compose, Two Services are defined in docker-compose.yaml
1. Mysql (also username and password defined there.)
2. Webapi
There is .env file in the root folder which configures web API, So if you want change connection credential use this file.

- install docker
- install docker-compose
- build image
```
docker-compose build
```

- run docker-compose file
```
docker-compose up -d
```
after running container you can access to APIs with **http://localhost:8000**

### Data Seeding
This project seeds some data into database Including 10 categories and 200 products, you can run it manually if you want more data;
*** for now seeders run by each deploy ***

1. execute Container
```
docker exec -ti plista_api sh
```

2. run seeds
```
php artisan db:seed
```

## APIs Doc
This project includes a Swagger-ui, So you can do API calls and check docs data, Normally the URL of documentation is **http://localhost:8000/api/documentation**
But in a case if you changed the URL you can access documentations by appending **/api/documentation** to end of base URL.

## TODO:
1. Use fpm and nginx  or php-apache Image as the base Image
2. Tests, Tests, Tests
3. Write a Response Handler
4. Write an Error Handler
5. Pagination for list products and list categories
6. Use Redis
