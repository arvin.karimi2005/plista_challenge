<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;


class CategoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->bind(
         'App\Services\Contracts\ICategoryService',
         'App\Services\CategoryService'
     );

    }
}
