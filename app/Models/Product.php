<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
  use HasFactory;


  protected $table = 'products';
  // protected $primaryKey = 'product_id';
  protected $fillable = ['name', 'price'];

    /**
     * Get the Product that belongs to a Category.
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
