<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $table = 'categories';
    // protected $primaryKey = 'category_id';
    protected $fillable = ['name'];

    /**
     * Get the Category.
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }


}
