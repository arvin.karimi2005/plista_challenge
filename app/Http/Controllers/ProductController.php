<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Services\Contracts\IProductService;

class ProductController extends BaseController
{

  protected $product;

  public function __construct(IProductService $productRepo){
      $this->product = $productRepo;
  }

  /**
     * @OA\Get(
     *      path="/product",
     *      operationId="getProductList",
     *      tags={"Products"},
     *      summary="Get list of Products",
     *      description="Returns list of Products",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *  )
     */
  public function list() {
    return $this->product->getAllProducts();
  }

  /**
     * @OA\Get(
     *      path="/product/{product_id}",
     *      operationId="getProduct",
     *   @OA\Parameter(name="product_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *      tags={"Products"},
     *      summary="Get product",
     *      description="Returns a product",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Category Not Found",
     *          @OA\JsonContent()
     *       ),
     *  )
     */
  public function get($productId){
    return $this->product->getProduct($productId);
  }


  /**
     * @OA\Post(
     *      path="/product",
     *      operationId="createProduct",
     *   @OA\RequestBody(
     *       required=true,
     *       description="Attributes of product",
     *       @OA\JsonContent(
     *          type="object",
     *          @OA\Property(property="name", type="string"),
     *          @OA\Property(property="price", type="number"),
     *          @OA\Property(property="category_id", type="string")
     *         )
     *      ),
     *      tags={"Products"},
     *      summary="Create a new product",
     *      description="Returns No Content",
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *  )
     */
  public function create(Request $request){

    $this->validate($request, [
      'name' => 'required|min:3|max:30',
      'price' => 'numeric',
      'category_id' => 'required'
    ]);

    $name = $request->get('name');
    $price = $request->get('price');
    $category_id = $request->get('category_id');

    $this->product->createProduct($name, $price, $category_id);
    return response("", 204);

  }

  /**
     * @OA\Put(
     *      path="/product/{product_id}",
     *      operationId="updateProduct",
     *   @OA\Parameter(name="product_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\RequestBody(
     *       required=true,
     *       description="Attributes of product",
     *       @OA\JsonContent(
     *          type="object",
     *          @OA\Property(property="name", type="string"),
     *          @OA\Property(property="price", type="number"),
     *          @OA\Property(property="category_id", type="string")
     *         )
     *      ),
     *      tags={"Products"},
     *      summary="Update a product",
     *      description="Returns No Content",
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Category Not Found",
     *          @OA\JsonContent()
     *       ),
     *  )
     */
  public function update(Request $request, $productId){
    $this->validate($request, [
      'name' => 'required|min:3|max:30'
    ]);

    $fields = [];

    $name = $request->get('name');
    $price = $request->get('price');
    $category_id = $request->get('category_id');

    if (!empty($name)) $fields['name'] = $name;
    if (!empty($price)) $fields['price'] = (double)$price;
    if (!empty($category_id)) $fields['category_id'] = $category_id;

    $this->product->updateProduct($productId, $fields);
    return response("", 204);

  }

  /**
     * @OA\Delete(
     *      path="/product/{product_id}",
     *      operationId="deleteProduct",
     *   @OA\Parameter(name="product_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *      tags={"Products"},
     *      summary="Delete product",
     *      description="Returns No Content",
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Category Not Found",
     *          @OA\JsonContent()
     *       ),
     *  )
     */
  public function delete($productId){
    $this->product->deleteProduct($productId);
    return response("", 204);

  }
}
