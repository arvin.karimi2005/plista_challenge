<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Services\Contracts\ICategoryService;

/**
 * @OA\Info(
 *    title="Your super  ApplicationAPI",
 *    version="1.0.0",
 * )
 */

class CategoryController extends BaseController
{

  protected $category;

  public function __construct(ICategoryService $categoryRepo) {
      $this->category = $categoryRepo;
  }

  /**
     * @OA\Get(
     *      path="/category",
     *      operationId="getCategoryList",
     *      tags={"Categories"},
     *      summary="Get list of categories",
     *      description="Returns list of categories",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *  )
     */
  public function list() {
    return $this->category->getAllCategories();
  }

  /**
     * @OA\Get(
     *      path="/category/{category_id}",
     *      operationId="getCategory",
     *   @OA\Parameter(name="category_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *      tags={"Categories"},
     *      summary="Get category",
     *      description="Returns a category",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Category Not Found",
     *          @OA\JsonContent()
     *       ),
     *  )
     */
  public function get($categoryId){
    return $this->category->getCategory($categoryId);
  }

  /**
     * @OA\Get(
     *      path="/category/{category_id}/products",
     *      operationId="getProducts",
     *   @OA\Parameter(name="category_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *      tags={"Categories"},
     *      summary="List products for a category",
     *      description="Returns list of products",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Category Not Found",
     *          @OA\JsonContent()
     *       ),
     *  )
     */
  public function getProducts($categoryId) {
      return $this->category->getProductsByCategory($categoryId);
  }

  /**
     * @OA\Post(
     *      path="/category",
     *      operationId="createCategory",
     *   @OA\RequestBody(
     *       required=true,
     *       description="name of category",
     *       @OA\JsonContent(
     *          type="object",
     *          @OA\Property(property="name", type="string")
     *         )
     *      ),
     *      tags={"Categories"},
     *      summary="Create a new category",
     *      description="Returns No Content",
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *  )
     */
  public function create(Request $request) {

    $this->validate($request, [
      'name' => 'required|min:3|max:30'
    ]);

    $category_name = $request->get('name');
    $this->category->createCategory($category_name);

    return response("", 204);
  }


  /**
     * @OA\Put(
     *      path="/category/{category_id}",
     *      operationId="updateCategory",
     *   @OA\Parameter(name="category_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\RequestBody(
     *       required=true,
     *       description="name of category",
     *       @OA\JsonContent(
     *          type="object",
     *          @OA\Property(property="name", type="string")
     *         )
     *      ),
     *      tags={"Categories"},
     *      summary="Update a category",
     *      description="Returns No Content",
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Category Not Found",
     *          @OA\JsonContent()
     *       ),
     *  )
     */
  public function update(Request $request, $categoryId) {
    $this->validate($request, [
      'name' => 'required|min:3|max:30'
    ]);

    $category_name = $request->get('name');

    $this->category->changeCategoryName($categoryId, $category_name);
    return response("", 204);
  }


  /**
     * @OA\Delete(
     *      path="/category/{category_id}/products",
     *      operationId="deleteCategory",
     *   @OA\Parameter(name="category_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *      tags={"Categories"},
     *      summary="Delete a category",
     *      description="Returns No Content",
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Category Not Found",
     *          @OA\JsonContent()
     *       ),
     *  )
     */
  public function delete($categoryId) {
    $this->category->deleteCategory($categoryId);
    return response("", 204);
  }
}
