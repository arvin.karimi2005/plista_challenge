<?php

namespace App\Services;

use App\Models\Product;
use App\Models\Category;

use App\Services\Contracts\IProductService;

/**
 *
 */
class ProductService implements IProductService
{

  public function getAllProducts($page=0, $limit=0) {
      return Product::all();
  }

  public function getProduct($productId) {
    return Product::with('category')->findOrFail($productId);
  }

  public function createProduct(string $name, float $price, $categoryId) {
    $category = Category::findOrFail($categoryId);
    $product = new Product();
    $product->name = $name;
    $product->price = $price;
    $product->category_id = $category->id;
    return $product->save();
  }

  public function updateProduct($productId, $fields) {
    //
    $product = Product::findOrFail($productId);

    if ( isset($fields['price']) && !empty($fields['name']) ) {
      $product->name = $fields['name'];
    }

    if ( isset($fields['price']) && !empty($fields['price']) ) {
      $product->price = $fields['price'];
    }

    if ( isset($fields['category_id']) && !empty($fields['category_id']) ) {
      $category = Category::findOrFail($fields['category_id']);
      $product->category_id = $fields['category_id'];
    }

    return $product->save();
  }

  public function deleteProduct($productId) {
    $product = Product::findOrFail($productId);
    return $product->delete();
  }

}
