<?php

namespace App\Services;

use App\Models\Category;
use App\Services\Contracts\ICategoryService;


/**
 *
 */
class CategoryService implements ICategoryService
{

  public function getAllCategories(){
      return Category::all();
  }

  public function getCategory($categoryId) {
    return Category::findOrFail($categoryId);
  }

  public function getProductsByCategory($categoryId) {
    $category = Category::findOrFail($categoryId);
    return $category->products()->get();
  }

  public function changeCategoryName($categoryId, string $name) {
    $category = Category::findOrFail($categoryId);
    $category->name = $name;
    return $category->save();
  }

  public function createCategory(string $name) {
    $category = new Category();
    $category->name = $name;
    return $category->save();
  }

  public function deleteCategory($categoryId) {
    $category = Category::findOrFail($categoryId);
    return $category->delete();
  }

}
