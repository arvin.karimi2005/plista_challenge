<?php

namespace App\Services\Contracts;

/**
 *  interface for Category Service
 */
interface ICategoryService
{
  public function getAllCategories();
  public function getCategory($categoryId);
  public function getProductsByCategory($categoryId);
  public function createCategory(string $name);
  public function changeCategoryName($categoryId, string $name);
  public function deleteCategory($categoryId);
}
