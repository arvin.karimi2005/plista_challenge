<?php

namespace App\Services\Contracts;

/**
 *  interface for Product Service
 */
interface IProductService
{
  public function getAllProducts($page=0, $limit=0);
  public function getProduct($productId);
  public function createProduct(string $name, float $price, $categoryId);
  public function updateProduct($productId, $fields);
  public function deleteProduct($productId);
}
