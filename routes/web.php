<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['prefix' => 'category'], function () use ($router) {
    $router->get('/', 'CategoryController@list');
    $router->get('/{categoryId}', 'CategoryController@get');
    $router->get('/{categoryId}/products', 'CategoryController@getProducts');
    $router->post('/', 'CategoryController@create');
    $router->put('/{categoryId}','CategoryController@update');
    $router->delete('/{categoryId}','CategoryController@delete');
});


$router->group(['prefix' => 'product'], function () use ($router) {
    $router->get('/', 'ProductController@list');
    $router->get('/{productId}', 'ProductController@get');
    $router->post('/', 'ProductController@create');
    $router->put('/{productId}','ProductController@update');
    $router->delete('/{productId}','ProductController@delete');
});
